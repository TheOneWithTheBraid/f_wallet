# v1.2.0

- chore(deps): bump com.android.application in /android (The one with the braid)
- chore(deps): bump go_router from 14.2.0 to 14.2.1 (The one with the braid)
- feat: add Lithuanian translations (The one with the braid)
- fix: remove NDK declaration (The one with the braid)

# v1.1.9

- chore(deps): bump pkpass from 2.1.1 to 2.1.2 (The one with the braid)
- chore: silent errors about missing webservice (The one with the braid)
- fix: improve PkPass rendering (The one with the braid)
- refactor: remove unnecessary SizedBox (The one with the braid)

# v1.1.8

- chore(deps): bump pkpass from 2.1.0 to 2.1.1 (The one with the braid)
- Translated using Weblate (Chinese (Simplified)) (porkifiableoinking)

# v1.1.7

- Added translation using Weblate (Chinese (Simplified)) (poi)
- Added translation using Weblate (Dutch) (Philip Goto)
- Added translation using Weblate (Estonian) (Priit Jõerüüt)
- Added translation using Weblate (Persian) (Sohrab Behdani)
- chore: add AUR link (The one with the braid)
- chore: add dependabot (The one with the braid)
- chore: bump dependabot (The one with the braid)
- chore(deps): bump org.jetbrains.kotlin:kotlin-gradle-plugin in /android (The one with the braid)
- chore: migrate Android embedder (The one with the braid)
- feat: add Contribution links (The one with the braid)
- fix: add missing proguard rules (The one with the braid)
- fix: CI job schedules (The one with the braid)
- fix: do not execute debug job on main (The one with the braid)
- fix: migrate CI stages (The one with the braid)
- fix: migrate CI stages (The one with the braid)
- fix: rename base locale CN (The one with the braid)
- refactor: make images gapless (The one with the braid)
- Translated using Weblate (Dutch) (Maartje Eyskens)
- Translated using Weblate (Dutch) (Maartje Eyskens)
- Translated using Weblate (Dutch) (Philip Goto)
- Translated using Weblate (Dutch) (Philip Goto)
- Translated using Weblate (Estonian) (Priit Jõerüüt)
- Translated using Weblate (Estonian) (Priit Jõerüüt)
- Translated using Weblate (Galician) (josé m)
- Translated using Weblate (German) (T. C)
- Translated using Weblate (Persian) (Sohrab Behdani)
- Translated using Weblate (Spanish) (gallegonovato)

# v1.1.6

- Added translation using Weblate (French) (The one with the braid)
- Added translation using Weblate (Galician) (josé m)
- Added translation using Weblate (Norwegian Bokmål) (Allan Nordhøy)
- add german translation (Florian Werner)
- chore: add weblate link (The one with the braid)
- feat: add locale chooser (The one with the braid)
- feat: make Linux metadata translatable (The one with the braid)
- fix: migrate desktop surface color (The one with the braid)
- fix: rename arb files to comply with Flutter standards (The one with the braid)
- Translated using Weblate (French) (Anonymous)
- Translated using Weblate (French) (The one with the braid)
- Translated using Weblate (Galician) (josé m)
- Translated using Weblate (German) (The one with the braid)
- Translated using Weblate (Norwegian Bokmål) (Allan Nordhøy)
- Translated using Weblate (Spanish) (Anonymous)
- Translated using Weblate (Spanish) (gallegonovato)

# v1.1.5

- chore: add monochrome icon (The one with the braid)
- chore: bump Dart PKPASS (The one with the braid)
- chore: migrate Flutter embedder to 3.22.2 (The one with the braid)
- chore: update dependencies (The one with the braid)
- chore: upgrade flutter to 3.22 (lauren n. liberda)
- feat: include appstream metadata (The one with the braid)
- feat: use localized value (Florian Werner)
- fix: codestyle (The one with the braid)
- fix: debian rules formatting (The one with the braid)
- fix: do not render GTK border (The one with the braid)

# v1.1.4

- fix: use correct WM startup class (The one with the braid)

# v1.1.3

- chore: bump pkpass (The one with the braid)
- chore: decrease size of screenshots (The one with the braid)
- chore: format metadata (The one with the braid)
- Created spanish translation (Diego)
- feat: add badges to README (The one with the braid)
- fix: add fallback background colors to passes (The one with the braid)
- fix: Debian icon names (The one with the braid)
- fix: debian launcher path (The one with the braid)
- fix: encode PkPass URI components (The one with the braid)
- fix: icon symbolic link (The one with the braid)

# v1.1.2

- chore: bump flutter (The one with the braid)
- chore: use custom Docker images (The one with the braid)
- chore: use platform-specific docker images (The one with the braid)
- feat: support cache for update checks (The one with the braid)
- fix: add missing Android network permission (The one with the braid)

# v1.1.1

- feat: add basic web service support (The one with the braid)
- fix: invalid filter on HTML content (The one with the braid)
- fix: use stable Debian on aarch64 (The one with the braid)

# v1.1.0

- chore: add Debian control files (The one with the braid)
- chore: add aarch64 Linux builds (The one with the braid)
- chore: add about dialog (The one with the braid)
- chore: improve README (The one with the braid)
- feat: add linter CI job (The one with the braid)
- feat: apply strict code style (The one with the braid)
- feat: implement PkPass share (The one with the braid)
- feat: support dynamic colors (The one with the braid)
- fix: Hero around Scaffold (The one with the braid)
- fix: add fallback colors for label field (The one with the braid)
- fix: enable multidex (The one with the braid)
- fix: escaped line breaks in PkPass files (The one with the braid)
- fix: supress intent error on desktops (The one with the braid)

# v1.0.6

- feat: make APK build reproducible

# v1.0.5

- feat: make APK build reproducible

# v1.0.4

- feat: make APK build reproducible

# v1.0.3

- feat: make APK build reproducible

# v1.0.2

- feat: pin Flutter version to file (The one with the braid)

# v1.0.1

- chore: add F-Droid metadata (The one with the braid)
- chore: add funding (The one with the braid)
- chore: add screenshots (The one with the braid)
- fix: typo in README (The one with the braid)
- fix: web deploy path (The one with the braid)
- fix: web deploy path (The one with the braid)

# v1.0.0

- chore: add CI (The one with the braid)
- chore: implement passs deletion (The one with the braid)
- chore: support DPI related scaling for PkPass assets (The one with the braid)
- chore: update README and add LICENSE (The one with the braid)
- CI: fix Android bundle path (The one with the braid)
- CI: fix Android bundle (The one with the braid)
- CI: fix Android keystore location (The one with the braid)
- CI: fix flutter-gen (The one with the braid)
- CI: fix Linux arch (The one with the braid)
- feat: add Linux cli support (The one with the braid)
- feat: add Linux desktop file (The one with the braid)
- feat: filter passes by category (The one with the braid)
- feat: simplify Android intents (The one with the braid)
- feat: sort passes by date (The one with the braid)
- feat: support expiry date time (The one with the braid)
- feat: support link open (The one with the braid)
- feat: support receiving sharing intent on Android (The one with the braid)
- feat: use system theme for splash screen (The one with the braid)
- fix: Android intent filter (The one with the braid)
- fix: Android intent single file (The one with the braid)
- fix: Android key decoding (The one with the braid)
- fix: file filter on Android (The one with the braid)
- fix: flicking (The one with the braid)
- fix: max width of logos in preview (The one with the braid)
- fix: pass deletion confirmation (The one with the braid)
