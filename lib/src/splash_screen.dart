import 'package:flutter/material.dart';

import 'package:animations/animations.dart';

import 'package:f_wallet/src/f_wallet_app.dart' deferred as app;
import 'package:f_wallet/src/utils/f_wallet_theme.dart';
import 'package:f_wallet/src/utils/hive_boxes.dart' deferred as hive;
import 'package:f_wallet/src/utils/secure_storage.dart' deferred as storage;
import 'utils/pass_manager.dart' deferred as pass;

class SplashScreen extends StatelessWidget {
  static final _secondaryNavigatorKey = GlobalKey<NavigatorState>();

  const SplashScreen({super.key});

  Future<bool> _initialize() async {
    await Future.wait([
      app.loadLibrary(),
      hive.loadLibrary(),
      storage.loadLibrary(),
      pass.loadLibrary(),
    ]);

    await storage.SecureStorage.init();
    await hive.HiveBoxes.init();
    await pass.PassManager.init();

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: _initialize(),
      builder: (context, snapshot) {
        Widget child;
        if (snapshot.hasData) {
          child = app.FWalletApp(
            key: const ValueKey(true),
          );
        } else {
          child = MaterialApp(
            title: 'fWallet',
            navigatorKey: _secondaryNavigatorKey,
            theme: FWalletTheme.light(),
            darkTheme: FWalletTheme.dark(),
            home: _LoadingScreen(
              key: ValueKey(snapshot.error),
              error: snapshot.error,
            ),
          );
        }
        return PageTransitionSwitcher(
          transitionBuilder: (child, primaryAnimation, secondaryAnimation) {
            return SharedAxisTransition(
              animation: primaryAnimation,
              secondaryAnimation: secondaryAnimation,
              transitionType: SharedAxisTransitionType.scaled,
              child: child,
            );
          },
          child: child,
        );
      },
    );
  }
}

class _LoadingScreen extends StatelessWidget {
  final Object? error;

  const _LoadingScreen({super.key, this.error});

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child: error == null
              ? const CircularProgressIndicator()
              : Text('Error opening fWallet: $error'),
        ),
      );
}
