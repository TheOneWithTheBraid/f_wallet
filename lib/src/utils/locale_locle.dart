import 'package:flutter/rendering.dart' as flutter;

import 'package:intl/locale.dart' as intl;

extension LocaleLocale on flutter.Locale {
  intl.Locale toLocale() => intl.Locale.fromSubtags(
        languageCode: languageCode,
        scriptCode: scriptCode,
        countryCode: countryCode,
      );
}
