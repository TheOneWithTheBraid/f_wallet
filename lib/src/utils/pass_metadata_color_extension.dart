import 'package:flutter/material.dart';

import 'package:pkpass/pkpass.dart';

extension PassMetadataColorExtension on PassMetadata {
  Color? get background => _maybeColor(backgroundColor);

  Color? get foreground => _maybeColor(foregroundColor);

  Color? get label => _maybeColor(labelColor);

  Color? surface(Brightness brightness) {
    Color? surface = background;
    final color = foreground ?? label;
    if (surface == null && color != null) {
      final scheme = ColorScheme.fromSeed(
        seedColor: color,
        brightness: brightness,
      );
      if (brightness == Brightness.dark) {
        surface = scheme.surface;
      } else {
        surface = scheme.tertiary;
      }
    }
    return surface;
  }

  Color? _maybeColor(int? color) {
    if (color == null) return null;
    return Color(color);
  }
}
