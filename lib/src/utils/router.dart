import 'dart:developer';

import 'package:go_router/go_router.dart';
import 'package:pkpass/pkpass.dart';

import 'package:f_wallet/src/pages/pass_details/pass_details.dart';
import 'package:f_wallet/src/pages/pass_list/pass_list.dart';
import 'package:f_wallet/src/utils/pass_manager.dart';
import 'package:f_wallet/src/widgets/desktop_single_page.dart';

final fWalletRouter = GoRouter(
  routes: [
    ShellRoute(
      builder: (context, state, child) {
        return DesktopSinglePage(
          child: child,
        );
      },
      routes: [
        GoRoute(path: '/', builder: (context, state) => const PassList()),
        GoRoute(
          path: '/pass/:pass',
          builder: (context, state) {
            final serialNumber =
                Uri.decodeComponent(state.pathParameters['pass']!);
            final pass =
                PassManager.instance().passFiles[serialNumber] as PassFile;
            return PassDetails(pass: pass);
          },
          redirect: (context, state) {
            final serialNumber =
                Uri.decodeComponent(state.pathParameters['pass']!);
            if (!PassManager.instance().passFiles.containsKey(serialNumber)) {
              log('Pass not found: $serialNumber');
              return '/';
            }
            return null;
          },
        ),
      ],
    ),
  ],
);
