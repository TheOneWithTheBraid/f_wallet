import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'package:f_wallet/src/utils/hive_boxes.dart';

class ThemeButton extends StatelessWidget {
  const ThemeButton({super.key});

  static Box<Object?> get box => HiveBoxes.settings;

  static ThemeMode getThemeMode(Box<Object?> box) {
    final themeSetting = box.get('darkTheme') as bool?;
    switch (themeSetting) {
      case true:
        return ThemeMode.dark;
      case false:
        return ThemeMode.light;
      default:
        return ThemeMode.system;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box<Object?>>(
      valueListenable: box.listenable(),
      builder: (context, box, child) {
        return IconButton(
          tooltip: AppLocalizations.of(context)!.theme,
          onPressed: _toggleTheme,
          icon: Icon(
            switch (getThemeMode(box)) {
              ThemeMode.system => Icons.auto_mode,
              ThemeMode.dark => Icons.dark_mode,
              ThemeMode.light => Icons.light_mode,
            },
          ),
        );
      },
    );
  }

  Future<void> _toggleTheme() async {
    final newTheme = switch (getThemeMode(box)) {
      ThemeMode.system => true,
      ThemeMode.dark => false,
      ThemeMode.light => null,
    };

    await box.put('darkTheme', newTheme);
  }
}
