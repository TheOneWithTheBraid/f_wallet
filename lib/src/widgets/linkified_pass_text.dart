import 'dart:developer';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:html/dom.dart' as dom;
import 'package:html/parser.dart';
import 'package:url_launcher/url_launcher.dart';

class LinkifiedPassText extends StatelessWidget {
  final String text;
  final TextAlign? textAlign;

  const LinkifiedPassText(this.text, {super.key, required this.textAlign});

  @override
  Widget build(BuildContext context) {
    final textStyle = DefaultTextStyle.of(context).style;
    final linkStyle = textStyle.copyWith(
      decoration: TextDecoration.underline,
      fontWeight: FontWeight.bold,
    );

    // yes, that's cursed
    final text = this.text.replaceAll(RegExp(r'(\\r)?\\n'), '\n');

    final document = parse(text);

    final body = document.body;

    // looks like no HTML elements included, only linkifying
    if (body == null ||
        body.nodes.length == 1 && body.nodes.first is dom.Text) {
      return Linkify(
        text: text,
        options: const LinkifyOptions(
          humanize: false,
          defaultToHttps: true,
        ),
        onOpen: (link) {
          final uri = Uri.tryParse(link.url);
          if (uri != null) launchUrl(uri);
        },
        style: textStyle,
        linkStyle: linkStyle,
        textAlign: textAlign ?? TextAlign.start,
      );
    }
    // okay, we're dealing with a hell of HTML
    else {
      final spans = body.nodes
          .map((e) {
            // according to spec, only `Anchor` attributes are allowed
            // still, we additionally support :
            // - br
            // - em, strong, b
            if (e is dom.Element && e.localName == 'a') {
              final href = e.attributes['href'];
              GestureRecognizer? recognizer;
              if (href != null) {
                final uri = Uri.tryParse(href);
                if (uri != null) {
                  recognizer = TapGestureRecognizer()
                    ..onTap = () => launchUrl(uri);
                }
              }

              return TextSpan(
                text: e.text,
                recognizer: recognizer,
                style: linkStyle,
              );
            }
            if (e is dom.Element && e.localName == 'br') {
              return const TextSpan(text: '\n\n');
            } else if (e is dom.Element &&
                ['b', 'strong', 'em'].contains(e.localName)) {
              return TextSpan(
                text: e.text,
                style: const TextStyle(fontWeight: FontWeight.bold),
              );
            } else if (e is dom.Text) {
              return TextSpan(text: e.text);
            } else {
              log(
                'Unsupported HTML node found: $e. Ignoring.',
                name: 'LinkifiedPassText',
              );
            }
          })
          .whereType<InlineSpan>()
          .toList();
      return SelectableText.rich(
        TextSpan(children: spans),
        textAlign: textAlign,
      );
    }
  }
}
