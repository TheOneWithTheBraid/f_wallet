import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'package:f_wallet/src/utils/hive_boxes.dart';
import 'language_dialog.dart';

class LanguageButton extends StatefulWidget {
  const LanguageButton({super.key});

  @override
  State<LanguageButton> createState() => _LanguageButtonState();
}

class _LanguageButtonState extends State<LanguageButton> {
  static Box<Object?> get box => HiveBoxes.settings;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box<Object?>>(
      valueListenable: box.listenable(),
      builder: (context, box, child) {
        return IconButton(
          tooltip: AppLocalizations.of(context)!.language,
          onPressed: _showLanguageDialog,
          icon: const Icon(Icons.translate),
        );
      },
    );
  }

  Future<void> _showLanguageDialog() async {
    final response = await showAdaptiveDialog<LocaleResponse>(
      context: context,
      builder: (context) => const LanguageDialog(),
    );
    if (response == null) return;
    final locale = response.locale;

    await box.put('locale', locale?.toLanguageTag());
  }
}
