import 'package:flutter/material.dart';

class DesktopSinglePage extends StatelessWidget {
  static final _layoutKey = GlobalKey();

  final Widget child;
  static const kHugeDeviceBreakpoint = 1200.0;
  static final _border = BorderRadius.circular(32);

  const DesktopSinglePage({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      key: _layoutKey,
      builder: (context, constraints) {
        final backgroundColor = Theme.of(context).colorScheme.surfaceBright;
        Widget child;
        if (constraints.maxWidth > kHugeDeviceBreakpoint) {
          child = Material(
            key: const ValueKey(DesktopSinglePage),
            color: backgroundColor,
            child: Center(
              child: ConstrainedBox(
                constraints: const BoxConstraints(
                  maxWidth: kHugeDeviceBreakpoint,
                  maxHeight: 786,
                ),
                child: Material(
                  elevation: 8,
                  borderRadius: _border,
                  child: ClipRRect(
                    borderRadius: _border,
                    child: this.child,
                  ),
                ),
              ),
            ),
          );
        } else {
          child = Container(
            key: ValueKey(this.child),
            child: this.child,
          );
        }
        return child;
      },
    );
  }
}
