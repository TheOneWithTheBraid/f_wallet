import 'package:flutter/material.dart';

import 'package:animations/animations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import 'package:f_wallet/src/pages/pass_list/components/pass_preview.dart';
import 'package:f_wallet/src/pages/pass_list/pass_list.dart';
import 'package:f_wallet/src/utils/pass_manager.dart';
import 'package:f_wallet/src/widgets/theme_button.dart';
import '../../widgets/language_button.dart';
import 'components/pass_tab_destination.dart';

class PassListView extends StatelessWidget {
  final PassListController controller;

  const PassListView({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<PassManager>(
      valueListenable: Provider.of<PassManager>(context, listen: false),
      builder: (context, passManager, child) {
        final files = passManager.passFiles.values.toList();
        final filtered = controller.filterPassFiles(files);

        final activeTabs = controller.activeTabs(files);
        return Scaffold(
          appBar: AppBar(
            title: Text(AppLocalizations.of(context)!.passes),
            actions: [
              const LanguageButton(),
              const ThemeButton(),
              IconButton(
                onPressed: controller.showInfoDialog,
                icon: const Icon(Icons.info),
                tooltip: AppLocalizations.of(context)!.about,
              ),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: controller.importPassFile,
            tooltip: AppLocalizations.of(context)!.import,
            child: const Icon(Icons.file_open),
          ),
          body: PageTransitionSwitcher(
            key: ValueKey(files.length),
            reverse: controller.reversed,
            transitionBuilder: (child, primaryAnimation, secondaryAnimation) {
              return SharedAxisTransition(
                animation: primaryAnimation,
                secondaryAnimation: secondaryAnimation,
                transitionType: SharedAxisTransitionType.horizontal,
                child: child,
              );
            },
            child: ListView.builder(
              key: ValueKey(controller.currentTab),
              itemCount: filtered.length,
              itemBuilder: (context, index) {
                final metadata = filtered.elementAt(index);
                return PassPreview(
                  pass: metadata,
                );
              },
            ),
          ),
          bottomNavigationBar: files.isNotEmpty
              ? NavigationBar(
                  selectedIndex: activeTabs.indexOf(controller.currentTab),
                  destinations:
                      activeTabs.map((e) => PassTabDestination(e)).toList(),
                  onDestinationSelected: controller.setCurrentTab,
                )
              : null,
        );
      },
    );
  }
}
