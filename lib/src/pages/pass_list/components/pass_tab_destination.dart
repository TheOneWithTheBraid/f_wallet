import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'pass_tabs.dart';

class PassTabDestination extends StatelessWidget {
  final PassTabs? tab;

  const PassTabDestination(this.tab, {super.key});

  @override
  Widget build(BuildContext context) {
    switch (tab) {
      case PassTabs.boardingPass:
        return NavigationDestination(
          icon: const Icon(Icons.tram),
          label: AppLocalizations.of(context)!.travelPasses,
        );
      case PassTabs.coupon:
        return NavigationDestination(
          icon: const Icon(Icons.discount),
          label: AppLocalizations.of(context)!.coupons,
        );
      case PassTabs.eventTicket:
        return NavigationDestination(
          icon: const Icon(Icons.theater_comedy),
          label: AppLocalizations.of(context)!.eventTickets,
        );
      case PassTabs.generic:
        return NavigationDestination(
          icon: const Icon(Icons.qr_code),
          label: AppLocalizations.of(context)!.generic,
        );
      case PassTabs.storeCard:
        return NavigationDestination(
          icon: const Icon(Icons.loyalty),
          label: AppLocalizations.of(context)!.storeCards,
        );
      case null:
        return NavigationDestination(
          icon: const Icon(Icons.filter_alt_off),
          label: AppLocalizations.of(context)!.allPasses,
        );
    }
  }
}
