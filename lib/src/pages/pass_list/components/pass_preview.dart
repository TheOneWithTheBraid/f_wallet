import 'package:flutter/material.dart' hide Locale;

import 'package:go_router/go_router.dart';
import 'package:pkpass/pkpass.dart';
import 'package:url_launcher/link.dart';

import 'package:f_wallet/src/utils/locale_locle.dart';
import 'package:f_wallet/src/utils/pass_metadata_color_extension.dart';
import 'package:f_wallet/src/widgets/card_shape.dart';
import 'package:f_wallet/src/widgets/device_pixel_ratio_builder.dart';

class PassPreview extends StatelessWidget {
  final PassFile pass;
  final Animation<double> animation;

  const PassPreview({
    super.key,
    required this.pass,
    this.animation = const AlwaysStoppedAnimation<double>(1),
  });

  PassMetadata get metadata => pass.metadata;

  Uri get detailsUrl =>
      Uri.parse('/pass/${Uri.encodeComponent(metadata.serialNumber)}');

  @override
  Widget build(BuildContext context) {
    return Link(
      builder: (context, followLink) {
        final locale = Localizations.localeOf(context).toLocale();

        final theme = Theme.of(context);
        final surface =
            metadata.surface(theme.brightness) ?? theme.colorScheme.surface;

        final fallbackForeground =
            surface.computeLuminance() > 0.5 ? Colors.black : Colors.white;
        var label = metadata.label ?? metadata.foreground ?? fallbackForeground;

        return SizeTransition(
          sizeFactor: animation,
          child: Padding(
            padding: const EdgeInsets.all(12),
            child: Hero(
              tag: metadata.serialNumber,
              child: CardShape(
                color: surface,
                child: InkWell(
                  onTap: () => context.push(detailsUrl.path),
                  child: DevicePixelRatioBuilder(
                    builder: (context, pixelRatio) {
                      final logo = pass.getLogo(
                        locale: locale,
                        scale: pixelRatio.ceil(),
                      );

                      return ListTile(
                        title: Text(
                          metadata.getLocalizedDescription(pass, locale),
                        ),
                        textColor: label,
                        subtitle: Text(
                          metadata.getLocalizedOrganizationName(pass, locale),
                        ),
                        trailing: logo == null
                            ? null
                            : ConstrainedBox(
                                constraints:
                                    const BoxConstraints(maxWidth: 192),
                                child: Image.memory(
                                  logo,
                                  gaplessPlayback: true,
                                  semanticLabel: metadata.getLocalizedLogoText(
                                    pass,
                                    locale,
                                  ),
                                ),
                              ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ),
        );
      },
      uri: detailsUrl,
    );
  }
}
