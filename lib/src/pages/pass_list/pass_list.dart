import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:file_selector/file_selector.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_sharing_intent/flutter_sharing_intent.dart';
import 'package:flutter_sharing_intent/model/sharing_file.dart';
import 'package:go_router/go_router.dart';
import 'package:pkpass/pkpass.dart';
import 'package:url_launcher/link.dart';

import 'package:f_wallet/src/utils/args_parser.dart';
import 'package:f_wallet/src/utils/pass_manager.dart';
import 'package:f_wallet/src/utils/version.dart';
import 'components/pass_tabs.dart';
import 'pass_list_view.dart';

class PassList extends StatefulWidget {
  const PassList({super.key});

  @override
  State<PassList> createState() => PassListController();
}

class PassListController extends State<PassList> {
  final passManager = PassManager.instance();

  PassTabs? currentTab;
  bool reversed = false;

  static bool _initialFileImportDone = false;

  StreamSubscription<List<SharedFile>>? _intentDataStreamSubscription;

  @override
  void initState() {
    _importFileArgs();
    _importFileIntent();
    _listenFileIntent();
    super.initState();
  }

  @override
  void dispose() {
    _intentDataStreamSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PassListView(controller: this);
  }

  void setCurrentTab(int newIndex) {
    final tabs = activeTabs(passManager.passFiles.values.toList());
    final previousIndex = tabs.indexOf(currentTab);

    setState(() {
      reversed = previousIndex > newIndex;
      currentTab = tabs[newIndex];
    });
  }

  List<PassTabs?> activeTabs(List<PassFile> files) => [
        null,
        if (files.any((element) => element.metadata.boardingPass != null))
          PassTabs.boardingPass,
        if (files.any((element) => element.metadata.coupon != null))
          PassTabs.coupon,
        if (files.any((element) => element.metadata.eventTicket != null))
          PassTabs.eventTicket,
        if (files.any((e) => e.metadata.storeCard != null)) PassTabs.storeCard,
        if (files.any((element) => element.metadata.generic != null))
          PassTabs.generic,
      ];

  List<PassFile> filterPassFiles(List<PassFile> files) {
    List<PassFile> filtered;
    switch (currentTab) {
      case null:
        filtered = files;
      case PassTabs.boardingPass:
        filtered = files.where((e) => e.metadata.boardingPass != null).toList();
      case PassTabs.coupon:
        filtered = files.where((e) => e.metadata.coupon != null).toList();
      case PassTabs.eventTicket:
        filtered = files.where((e) => e.metadata.eventTicket != null).toList();
      case PassTabs.generic:
        filtered = files.where((e) => e.metadata.generic != null).toList();
      case PassTabs.storeCard:
        filtered = files.where((e) => e.metadata.storeCard != null).toList();
    }

    filtered.sort((a, b) {
      final dateA = a.metadata.relevantDate;
      final dateB = b.metadata.relevantDate;
      if (dateA != null && dateB != null) {
        return dateB.compareTo(dateA);
      } else if (dateA != null) {
        return -1;
      } else if (dateB != null) {
        return 1;
      }
      return 0;
    });
    return filtered;
  }

  Future<void> importPassFile() async {
    final group = XTypeGroup(
      label: 'PkPass',
      extensions: const <String>['pkpass', 'pkpasses'],
      mimeTypes: kIsWeb || Platform.isAndroid || Platform.isIOS
          ? null
          : [
              'application/vnd.apple.pkpass',
              'application/vnd.apple.pkpasses',
            ],
    );
    final List<XFile> files = await openFiles(acceptedTypeGroups: [group]);
    await _importXFiles(files);
  }

  Future<void> _importXFiles(List<XFile> files) async {
    final result = await Future.wait(
      files.map(
        (e) async => passManager.save(await e.readAsBytes()),
      ),
    );
    final serialNumber = result.singleOrNull;
    if (serialNumber != null) {
      if (mounted) await context.push(Uri.parse('/pass/$serialNumber').path);
    }
  }

  Future<void> _importFileArgs() async {
    if (_initialFileImportDone) return;
    _initialFileImportDone = true;
    if (!PassArgsParser.hasFiles) return;

    final files = PassArgsParser.files;
    await _importXFiles(files);
  }

  Future<void> _importIntentFileList(List<SharedFile> sharedFiles) async {
    final files = sharedFiles
        .map((e) {
          final path = e.value;
          if (path != null) return XFile(path);
        })
        .whereType<XFile>()
        .toList();

    if (files.isEmpty) return;
    _importXFiles(files);
  }

  Future<void> _importFileIntent() async {
    try {
      final initialContent =
          await FlutterSharingIntent.instance.getInitialSharing();
      _importIntentFileList(initialContent);
    } on MissingPluginException catch (_) {
      log(
        'Listening for incoming shares not supported on this platform.',
        name: 'IncomingShares',
      );
    }
  }

  void _listenFileIntent() async {
    try {
      _intentDataStreamSubscription = FlutterSharingIntent.instance
          .getMediaStream()
          .listen(_importIntentFileList);
    } on MissingPluginException catch (_) {}
  }

  void showInfoDialog() => showAboutDialog(
        context: context,
        applicationVersion: Version.version,
        applicationIcon: Image.asset(
          'assets/logo/logo-circle.png',
          gaplessPlayback: true,
          width: 64,
          height: 64,
        ),
        applicationLegalese:
            '${AppLocalizations.of(context)!.appSlogan}\n\n${AppLocalizations.of(context)!.author(Version.author)}',
        children: [
          Link(
            uri: Uri.parse(Version.gitlabRepoBase),
            builder: (context, followLink) {
              return OutlinedButton.icon(
                onPressed: followLink,
                icon: const Icon(Icons.public),
                label: Text(AppLocalizations.of(context)!.repoLabel),
              );
            },
          ),
          Link(
            uri: Version.isStable
                ? Uri.parse(Version.stableChangeLog)
                : Uri.parse(Version.commitList),
            builder: (context, followLink) {
              return OutlinedButton.icon(
                onPressed: followLink,
                icon: const Icon(Icons.list_alt),
                label: Text(AppLocalizations.of(context)!.releaseNotes),
              );
            },
          ),
          Link(
            uri: Uri.parse(Version.donationLink),
            builder: (context, followLink) {
              return OutlinedButton.icon(
                onPressed: followLink,
                icon: const Icon(Icons.coffee),
                label: Text(AppLocalizations.of(context)!.buyMeACoffee),
              );
            },
          ),
        ]
            .map(
              (e) => Padding(
                padding: const EdgeInsets.symmetric(vertical: 4),
                child: e,
              ),
            )
            .toList(),
      );
}
