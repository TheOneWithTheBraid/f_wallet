import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:pkpass/pkpass.dart';

import 'package:f_wallet/src/pages/pass_details/components/barcode.dart';
import 'package:f_wallet/src/pages/pass_details/pass_details.dart';
import 'package:f_wallet/src/utils/locale_locle.dart';
import 'package:f_wallet/src/utils/pass_metadata_color_extension.dart';
import 'package:f_wallet/src/widgets/device_pixel_ratio_builder.dart';
import 'components/pass_structure_view.dart';

class PassDetailsView extends StatelessWidget {
  final PassDetailsController controller;

  const PassDetailsView({
    super.key,
    required this.controller,
  });

  PassMetadata get metadata => controller.pass.metadata;

  @override
  Widget build(BuildContext context) {
    final locale = Localizations.localeOf(context).toLocale();

    final surface = metadata.surface(Theme.of(context).brightness) ??
        Theme.of(context).colorScheme.surface;
    final fallbackForeground =
        surface.computeLuminance() > 0.5 ? Colors.black : Colors.white;

    final color = metadata.foreground ?? metadata.label ?? fallbackForeground;
    final label = metadata.label ?? metadata.foreground ?? fallbackForeground;

    final boardingPass = metadata.boardingPass;
    final coupon = metadata.coupon;
    final eventTicket = metadata.eventTicket;
    final storeCard = metadata.storeCard;
    final generic = metadata.generic;

    return DevicePixelRatioBuilder(
      builder: (context, pixelRatio) {
        final scale = pixelRatio.ceil();
        final logo = controller.pass.getLogo(
          locale: locale,
          scale: scale,
        );
        final footer = controller.pass.getFooter(
          locale: locale,
          scale: scale,
        );
        return Scaffold(
          backgroundColor: surface,
          appBar: AppBar(
            leading: const BackButton(),
            iconTheme: IconThemeData(color: label),
            backgroundColor: Colors.transparent,
            title: Text(
              metadata.getLocalizedOrganizationName(controller.pass, locale),
              style: TextStyle(color: label),
            ),
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(48),
              child: ListTile(
                title: Text(
                  metadata.getLocalizedDescription(controller.pass, locale),
                  style: TextStyle(color: label),
                ),
              ),
            ),
            actions: [
              if (logo != null)
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: Image.memory(
                    logo,
                    gaplessPlayback: true,
                    semanticLabel: metadata.getLocalizedLogoText(
                      controller.pass,
                      locale,
                    ),
                  ),
                ),
              IconButton(
                onPressed: controller.delete,
                icon: const Icon(Icons.delete),
                tooltip: AppLocalizations.of(context)!.delete,
              ),
              // builder required for iPasOS share
              Builder(
                builder: (context) {
                  return IconButton(
                    onPressed: () => controller.share(context),
                    icon: const Icon(Icons.share),
                    tooltip: AppLocalizations.of(context)!.share,
                  );
                },
              ),
              if (controller.pass.metadata.webService != null)
                IconButton(
                  onPressed: controller.refreshPass,
                  icon: const Icon(Icons.refresh),
                  tooltip: AppLocalizations.of(context)!.update,
                ),
            ],
          ),
          body: Hero(
            tag: metadata.serialNumber,
            child: Material(
              color: Colors.transparent,
              child: IconTheme(
                data: IconThemeData(color: color),
                child: ListTileTheme(
                  iconColor: color,
                  textColor: color,
                  child: DefaultTextStyle(
                    style: TextStyle(color: color),
                    child: RefreshIndicator(
                      onRefresh: controller.refreshPass,
                      child: ListView(
                        children: [
                          ...metadata.barcodes.map(
                            (e) => BarcodeView(
                              barcode: e,
                              color: color,
                            ),
                          ),
                          ...[
                            boardingPass,
                            coupon,
                            eventTicket,
                            storeCard,
                            generic,
                          ]
                              .map(
                                (e) => e == null
                                    ? null
                                    : PassStructureView(
                                        dictionary: e,
                                        file: controller.pass,
                                      ),
                              )
                              .whereType<Widget>(),
                          if (footer != null)
                            Image.memory(
                              footer,
                              gaplessPlayback: true,
                            ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
