import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:file_selector/file_selector.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pkpass/pkpass.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:f_wallet/src/pages/pass_details/pass_details_view.dart';
import 'package:f_wallet/src/utils/hive_boxes.dart';
import 'package:f_wallet/src/utils/locale_locle.dart';
import 'package:f_wallet/src/utils/pass_manager.dart';

class PassDetails extends StatefulWidget {
  final PassFile pass;

  const PassDetails({super.key, required this.pass});

  @override
  State<PassDetails> createState() => PassDetailsController();
}

class PassDetailsController extends State<PassDetails> {
  PassFile? _updatedPassFile;

  PassFile get pass => _updatedPassFile ?? widget.pass;

  Future<void> share(BuildContext c) async {
    final box = c.findRenderObject() as RenderBox?;
    final appleOrigin = box!.localToGlobal(Offset.zero) & box.size;
    final locale = Localizations.localeOf(context).toLocale();
    final bytes = await HiveBoxes.passes.get(pass.metadata.serialNumber);
    if (bytes == null) {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(AppLocalizations.of(context)!.couldNotShare)),
        );
      }
      return;
    }
    final organization =
        pass.metadata.getLocalizedOrganizationName(pass, locale);
    final description = pass.metadata.getLocalizedDescription(pass, locale);
    final name = '$organization - $description.pkpass';
    final xfile = XFile.fromData(
      bytes,
      name: name,
      mimeType: 'application/vnd.apple.pkpass',
      length: bytes.length,
    );
    if (kIsWeb) {
      // storing files on web is quite broken, yes
      launchUrl(
        Uri.parse(
          "data:application/vnd.apple.pkpass;base64,${base64Encode(bytes)}",
        ),
      );
    } else if (Platform.isAndroid || Platform.isIOS) {
      await Share.shareXFiles([xfile], sharePositionOrigin: appleOrigin);
    } else {
      final download = await getDownloadsDirectory();
      const filter = XTypeGroup(
        label: 'PkPass',
        extensions: ['pkpass'],
        mimeTypes: [
          'application/vnd.apple.pkpass',
        ],
      );
      final location = await getSaveLocation(
        acceptedTypeGroups: [filter],
        initialDirectory: download?.path,
        suggestedName: xfile.name,
      );
      if (location == null) {
        if (mounted) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(AppLocalizations.of(context)!.couldNotShare),
            ),
          );
        }
        return;
      }
      final path = location.path;
      final file = File(path);
      await file.create(recursive: true);
      await file.writeAsBytes(bytes);

      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context)!.savedTo(path)),
          ),
        );
      }
      return;
    }
  }

  Future<void> delete() async {
    final locale = Localizations.localeOf(context).toLocale();
    final delete = await showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          AppLocalizations.of(context)!.deletePass(
            pass.metadata.getLocalizedDescription(pass, locale),
          ),
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text(AppLocalizations.of(context)!.cancel),
          ),
          TextButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: Text(AppLocalizations.of(context)!.delete),
          ),
        ],
      ),
    );
    if (delete != true) return;

    final manager = PassManager.instance();
    await manager.delete(pass.metadata.serialNumber);

    if (mounted) {
      Navigator.of(context).pop();
    }
  }

  Future<void> refreshPass() async {
    final update = await PassManager.instance().checkForUpdate(
      pass.metadata.serialNumber,
    );
    switch (update) {
      case null:
        if (mounted) {
          setState(() {});

          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(AppLocalizations.of(context)!.errorUpdatingPass),
            ),
          );
        }
      case true:
        _updatedPassFile =
            PassManager.instance().passFiles[pass.metadata.serialNumber];
        if (mounted) {
          setState(() {});

          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(AppLocalizations.of(context)!.passUpdated)),
          );
        }
        break;
      case false:
        if (mounted) {
          setState(() {});

          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(AppLocalizations.of(context)!.passIsLatest)),
          );
        }
        break;
    }
  }

  @override
  Widget build(BuildContext context) => PassDetailsView(controller: this);
}
