import 'package:flutter/material.dart';

import 'package:barcode_widget/barcode_widget.dart';
import 'package:pkpass/pkpass.dart';

class BarcodeView extends StatelessWidget {
  final PassBarcode barcode;
  final Color? color;

  const BarcodeView({super.key, required this.barcode, this.color});

  @override
  Widget build(BuildContext context) {
    final color = this.color ?? Theme.of(context).colorScheme.onSurface;

    final altText = barcode.altText;
    return Center(
      child: DecoratedBox(
        decoration: BoxDecoration(
          border: Border.all(color: color, width: 2),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              BarcodeWidget.fromBytes(
                data: barcode.barcodeData,
                barcode: Barcode.fromType(barcode.format),
                color: color,
                width: 256,
                height: [BarcodeType.QrCode, BarcodeType.Aztec]
                        .contains(barcode.format)
                    ? 256
                    : 128,
              ),
              if (altText != null)
                Padding(
                  padding: const EdgeInsets.only(top: 12.0),
                  child: SelectableText(altText),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
