import 'package:flutter/material.dart';

import 'package:pkpass/pkpass.dart';

import 'package:f_wallet/src/pages/pass_details/components/dictionary_field_view.dart';
import 'package:f_wallet/src/pages/pass_details/components/transit_type_icon.dart';

class PassStructureView extends StatelessWidget {
  final PassStructureDictionary dictionary;
  final PassFile file;

  const PassStructureView({
    super.key,
    required this.dictionary,
    required this.file,
  });

  @override
  Widget build(BuildContext context) {
    final transitType = dictionary.transitType;
    return SelectionArea(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          TransitTypeIcon(
            transitType: transitType,
            relevantDate: file.metadata.relevantDate,
            expirationDate: file.metadata.expirationDate,
          ),
          ...[
            dictionary.headerFields,
            dictionary.primaryFields,
            dictionary.secondaryFields,
            dictionary.backFields,
            dictionary.auxiliaryFields,
          ]
              .map(
                (fields) => fields.isNotEmpty
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: fields
                              .map(
                                (e) =>
                                    DictionaryFieldView(field: e, file: file),
                              )
                              .toList(),
                        ),
                      )
                    : null,
              )
              .whereType<Widget>(),
        ],
      ),
    );
  }
}
